/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.uchicago.CamelGroup;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.component.jms.JmsComponent;
import javax.jms.ConnectionFactory;

public class Translator {

    public static void main(String args[]) throws Exception {
        // create CamelContext
        CamelContext context = new DefaultCamelContext();

        // connect to ActiveMQ JMS broker listening on localhost on port 61616
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");
        context.addComponent("jms", JmsComponent.jmsComponentAutoAcknowledge(connectionFactory));

        // add our route to the CamelContext
        context.addRoutes(new RouteBuilder() {
            public void configure() {
                from("file:data/inbox/translator?noop=true")
                        .process(new Processor() {
                            public void process(Exchange e) throws Exception {
                                //Get the body of the message
                                String body = e.getIn().getBody(String.class);

                                //Translate the body
                                String newBody =  translateMsg(body);

                                //Set the body of the out message
                                e.getOut().setBody(newBody);

                            }// end process method

                        }) //end .process()

                        .to("file:data/outbox?fileName=${body}.out");




                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        // start the route, let it run for 5 sec, stop it
        context.start();
        Thread.sleep(5000);
        context.stop();
    }

    //method that translates 1337 to normal
    public static String translateMsg(String linetoTranslate) {
        StringBuilder newLine = new StringBuilder();
        for (int i=0; i < linetoTranslate.length(); i++) {
            char letter = linetoTranslate.charAt(i);
            char newChar = ' ';
            switch (letter) {
                case '1':
                    newChar = 'l';
                    break;
                case '3':
                    newChar = 'e';
                    break;
                case '4':
                    newChar = 'a';
                    break;
                case '5':
                    newChar = 's';
                    break;
                case '7':
                    newChar = 't';
                    break;
                case '8':
                    newChar = 'b';
                    break;
                case '0':
                    newChar = 'o';
                    break;
                case ' ':
                    newChar = '-';
                    break;
                default:
                    newChar = letter;
            }  //end switch

            newLine.append(newChar);

        } //end for

        return newLine.toString();
    }
}
